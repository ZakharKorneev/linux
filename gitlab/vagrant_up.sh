#!/bin/bash
set -e -x

scp -r -o StrictHostKeyChecking=no $REMOTE_WORK_DIR $TARGET_HOST:$PROJECT_LOCATE

ssh -o StrictHostKeyChecking=no "${TARGET_HOST}" << EOT
  /bin/bash

  set -x -e

  cd ${REMOTE_WORK_DIR}
  vagrant up
EOT
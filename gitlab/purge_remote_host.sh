#!/bin/bash
set -e -x

ssh -o StrictHostKeyChecking=no "${TARGET_HOST}" << EOT
  /bin/bash

  set -x -e

  cd ${REMOTE_WORK_DIR}
  vagrant destroy -f
  cd ../
  rm -rf ${REMOTE_WORK_DIR}
EOT
# Домашнее задание
Работа с загрузчиком

## Цель:
- Научиться попадать в систему без пароля;
- Устанавливать систему с LVM и переименовывать в VG.

## Задание
- [x] Включить отображение меню Grub.
- [x] Попасть в систему без пароля несколькими способами.
- [x] Установить систему с LVM, после чего переименовать VG.

# Шаги выполнения

## 1. Включить отображение меню Grub
Чтобы включить отображение меню Grub, нам нужно изменить конфигурационный файл `/etc/default/grub`. 
Мы можем сделать это с помощью команды `sudo nano /etc/default/grub` и вписать строчки `GRUB_TIMEOUT=10`, `GRUB_TIMEOUT_STYLE=menu`
![edited_grub_cfg.png](edited_grub_cfg.png)

## 2. Попасть в систему без пароля несколькими способами
### 2.1 Изменить параметры загрузки во время загрузки ОС
![edited_boot_parameters.png](edited_boot_parameters.png)

- `rw` - разрешает писать и удалять данные с диска;
- `init=/bin/bash` - загружаемся сразу в командную оболочку.

После сохраняем изменения `Ctrl+X` и загружаемся

![root_user.png](root_user.png)

### 2.2 Через recovery mode

В меню загрузки выбираем `*Advanced options for Ubuntu` -> `Ubuntu... (recovery mode)`

![recovery_mode.png](recovery_mode.png)

После загрузки получаем доступ к root

![root_user_recovery_mode.png](root_user_recovery_mode.png)

### 2.3 Различие в вариантах получения шелл
1. Редактирование GRUB с опцией init=/bin/bash
Ядро грузится с использованием /bin/bash в качестве основного процесса (init).

- Что происходит:

В нормальных условиях загрузки Linux, первым процессом, который запускается после загрузки ядра, является init (или его современная замена, как systemd). Этот процесс отвечает за запуск всех остальных процессов в системе.
Когда мы указываем init=/bin/bash, система пропускает этот этап и сразу запускает оболочку bash в качестве init-процесса.
Это означает, что никаких других сервисов и демонов не запускается и есть только голый shell с минимальным окружением.
Файловая система может быть смонтирована в режиме только для чтения, что потребует ее перемонтирования (mount -o remount,rw /), если нужно вносить изменения.

- Плюсы и минусы:

Плюсы: Очень быстрый способ попасть в shell. Подходит для исправления критических ошибок в конфигурации системы.

Минусы: Никакие службы не запущены, инициализация системы не выполнена. Требуется больше ручной работы (например, монтирование файловых систем).

2. Recovery Mode (режим восстановления)

Режим восстановления доступен через меню GRUB и предназначен для решения проблем с загрузкой системы. В этом режиме система загружается с минимальным набором сервисов и предоставляет shell с повышенными привилегиями (root).

- Что происходит:

В режиме восстановления запускается сокращённый init-процесс, который загружает только основные системные компоненты, необходимые для диагностики и исправления проблем.
Shell запускается после минимальной инициализации системы, что означает, что базовые сервисы и файловые системы будут смонтированы.

- Плюсы и минусы:

Плюсы: Более удобный доступ к исправлению проблем, так как система уже частично инициализирована. Не требует ручного монтирования файловых систем.

Минусы: Если "базовая" система не загружалась из-за проблем в системных компонентах, то и recovery mode тоже не загрузится

3. Сравнение

GRUB с init=/bin/bash: Более «низкоуровневый» метод, запускает только оболочку без каких-либо системных служб. Подходит когда система "сильно разломана" или задеты системные службы
Recovery Mode: Больше функций и больше окружения, подходит для большинства случаев восстановления системы.

Таким образом, выбор метода зависит от конкретной ситуации и задач.

## 3. Установить систему с LVM, после чего переименовать VG.

Переименовываем Volume Group:
```commandline
vgrename ubuntu-vg ubuntu-otus
```

Изменяем файл конфигурации grub для работы с новой группой
```commandline
sed -i 's/ubuntu--vg/ubuntu--otus/g' /boot/grub/grub.cfg
```

![rename_vg.png](rename_vg.png)

Перезагружаем систему и проверяем, что изменения применились:

![after_reboot.png](after_reboot.png)

import random
import string
from datetime import datetime
from pathlib import Path

PATH_TO_LOGS = Path('/var/log/')


def generate_random_string(length: int) -> str:
    characters = string.ascii_letters + string.digits + string.whitespace
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string


def generate_log(length_of_string: int) -> str:
    random_string = generate_random_string(length_of_string)
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ': ' + random_string


def generate_alert() -> str:
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ': ' + 'ALERT'


def save_log_to_file(log_data: str, filename: str) -> None:
    file_path = PATH_TO_LOGS / filename

    # Создание директорий, если их нет
    file_path.parent.mkdir(parents=True, exist_ok=True)

    with open(file_path, 'a') as file:
        file.write(log_data + '\n')


if __name__ == '__main__':
    for _ in range(600):
        increment = int(random.choice(string.digits))
        str_length: int = 10 + increment
        if increment == 0:
            log = generate_alert()
        else:
            log = generate_log(str_length)
        save_log_to_file(log_data=log, filename='watchlog.log')
    print('All done!!!')

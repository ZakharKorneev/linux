#!/bin/bash

echo 'Remove the old logical volume'
if lvremove -y /dev/VolGroup00/LogVol00; then
    echo "Logical volume removed successfully."
else
    echo "Failed to remove the logical volume."
    exid 1
fi

echo 'Create a new 8G logical volume'
if lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00; then
    echo "Logical volume created successfully."
else
    echo "Failed to create the logical volume."
    exit 1
fi

echo 'Create a new file system on 8G volume'
if mkfs.xfs /dev/VolGroup00/LogVol00; then
    echo "File system created successfully."
else
    echo "Failed to create the file system."
    exit 1
fi

echo 'Mount a 8G volume'
if mount /dev/VolGroup00/LogVol00 /mnt
    then
        echo "Mounted successfully."
        df -Th | grep -v tmpfs
    else
        echo "Error mounting."
        exit 1
fi


echo 'Backup "/" to 8G volume'
if xfsdump -J - /dev/vg_root/lv_root | xfsrestore -J - /mnt
    then
        echo 'Backup completed successfully.'
        ls /mnt
    else
        echo 'Error during backup.'
        exit 1
fi

echo 'Set GRUB config'
for i in /proc/ /sys/ /dev/ /run/ /boot/
do
    mount --bind $i /mnt/$i
done

chroot /mnt/

sed -i 's/rd.lvm.lv=vg_root\/lv_root/rd.lvm.lv=VolGroup00\/LogVol00/g' /etc/default/grub
sed -i 's/GRUB_DEFAULT=saved/GRUB_DEFAULT=1/g' /etc/default/grub

grub2-mkconfig -o /boot/grub2/grub.cfg

cd /boot || exit 1

for i in `ls initramfs-*img`; do
  dracut -v $i `echo $i|sed "s/initramfs-//g; > s/.img//g"` --force
done
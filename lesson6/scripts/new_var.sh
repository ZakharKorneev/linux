#!/bin/bash

# Script for creating a logical volume and mounting it to /mnt, then copying var data to new LV, unmounting, and finally configuring fstab for persistent mount.

echo "Create physical volumes from specified block devices"
pvcreate /dev/sdb /dev/sdd

echo "Create a volume group named vg_var using the created physical volumes"
vgcreate vg_var /dev/sdc /dev/sdd

echo "Create a logical volume (LV) named lv_var within the vg_var, with size 950M and mirroring for redundancy"
lvcreate -L 950M -m1 -n lv_var vg_var

echo "Format the newly created LV as ext4 file system"
mkfs.ext4 /dev/vg_var/lv_var

echo "Mount the logical volume (LV) to /mnt directory"
mount /dev/vg_var/lv_var /mnt

echo "Copy all contents from existing var directory to the newly mounted LV at /mnt"
cp -aR /var/* /mnt/

echo "Unmount the /mnt directory where var data was temporarily stored"
umount /mnt

echo "Mount the logical volume (LV) back to its original mount point, which is now configured as /var"
mount /dev/vg_var/lv_var /var

echo "Add an entry to fstab for automatically mounting the LV at /var upon boot using blkid and grep commands to find UUID of var filesystem"
echo "$(blkid | grep var: | awk '{print $2}') /var ext4 defaults 0 0" >> /etc/fstab

exit 0

#!/bin/bash

echo 'Create a new partition on the device /dev/sdb'
if pvcreate /dev/sdb
    then
        echo "Partition created successfully."
    else
        echo "Error creating partition."
        exit 1
fi


echo 'Create a new volume group on the device /dev/sdb'
if vgcreate vg_root /dev/sdb
    then
        echo 'Volume group created successfully.'
    else
        echo 'Volume group was not created'
    exit 1
fi

echo 'Create a new partition on the device /dev/sdb'
if lvcreate -n lv_root -l +100%FREE /dev/vg_root
    then
        echo 'Logical volume created successfully.'
    else
        echo 'Logical volume was not created'
        exit 1
fi

echo 'Create xfs file system.'
if mkfs.xfs /dev/vg_root/lv_root
    then
        echo 'Filesystem created successfully.'
    else
        echo 'Filesystem was not created'
        exit 1
fi

echo 'Mount a new volume'
if mount /dev/vg_root/lv_root /mnt
    then
        echo "Mounted successfully."
        df -Th | grep -v tmpfs
    else
        echo "Error mounting."
        exit 1
fi

echo 'Backup "/"'
if xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt
    then
        echo 'Backup completed successfully.'
        ls /mnt
    else
        echo 'Error during backup.'
        exit 1
fi

echo 'Set GRUB config'
for i in /proc/ /sys/ /dev/ /run/ /boot/
do
    mount --bind $i /mnt/$i
done

chroot /mnt/

sed -i 's/rd.lvm.lv=VolGroup00\/LogVol00/rd.lvm.lv=vg_root\/lv_root/g' /etc/default/grub
sed -i 's/GRUB_DEFAULT=saved/GRUB_DEFAULT=1/g' /etc/default/grub

grub2-mkconfig -o /boot/grub2/grub.cfg

cd /boot || exit 1

for i in `ls initramfs-*img`; do
  dracut -v $i `echo $i|sed "s/initramfs-//g; > s/.img//g"` --force
done

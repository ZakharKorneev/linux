#!/bin/bash

# This script is designed to remove an LVM setup for a root filesystem.

purge_lvm() {
    echo "Remove the logical volume lv_root associated with the volume group vg_root"
    lvremove -y /dev/vg_root/lv_root

    echo "Remove the entire volume group vg_root"
    vgremove /dev/vg_root

    echo "Remove the physical volume, which was part of the LVM setup"
    pvremove "$(pvs | grep -v VolGroup00 | awk '{print $1}' | grep dev)"
}

if purge_lvm; then
    "LVM setup /dev/vg_root/lv_root was remove"
else
    echo "Failed to remove the LVM setup /dev/vg_root/lv_root."
    exit 1
fi

exit 0

#!/bin/bash

# This script creates a logical volume for home directory, formats it, mounts it, copies home directory contents, and then makes the new mount point persistent.

echo "Create a logical volume named LogVol_Home with a size of 2G from VolGroup00"
lvcreate -n LogVol_Home -L 2G /dev/VolGroup00

echo "Format the newly created logical volume as xfs filesystem"
mkfs.xfs /dev/VolGroup00/LogVol_Home

echo "Mount the new logical volume to a temporary mount point /mnt"
mount /dev/VolGroup00/LogVol_Home /mnt/

echo "Copy all contents of /home directory to the newly mounted /mnt directory"
cp -aRv /home/* /mnt/

echo "Unmount the temporary mount point /mnt"
rm -rf /home/*
umount /mnt

echo "Mount the logical volume back to the original home directory location"
mount /dev/VolGroup00/LogVol_Home /home/

echo "Add an entry to /etc/fstab for making this mount persistent across reboots. This uses the UUID of LogVol_Home to ensure correct mapping."
echo "$(blkid | grep Home | awk '{print $2}') /home xfs defaults 0 0" >> /etc/fstab

exit 0

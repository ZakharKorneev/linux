#!/bin/bash

LOCKFILE="/tmp/send_email.lock" # Путь к файлу блокировки
LAST_LOG_DATE="last_log_date.txt" # Путь к файлу с датой последнего обработанного лога
MESSAGE="/tmp/message.txt" # Путь к файлу сообщения для отправки по email
EMAIL="your_email@example.com" # Переменная для указания адреса получателя

# Проверяем наличие файла блокировки
if [ -e "$LOCKFILE" ]; then
    echo "Скрипт уже выполняется. Завершаем выполнение."
    exit 1
fi

# Проверяем наличие времени обработки последнего лога
OLD_DATE=$(cat "$LAST_LOG_DATE" 2> /dev/null) # Получаем время обработки последнего лога
# Если время последнего обработанного лога не получили - назначаем "нулевое" время
if [[ -z "$OLD_DATE" ]]; then
  OLD_DATE=$(date --date='@0')
fi
# Функция для удаления временных файлов
del_lock() {
    rm -f $LOCKFILE
    rm -f $MESSAGE
}

# Устанавливаем ловушку на удаление временных файлов при выходе или прерывании скрипта
trap del_lock EXIT

# Создаем файл блокировки
touch "$LOCKFILE"

# Получаем файлы для обработки
LOGS_FILES=$(find . -type f -name "access-*\.log")

# Получить логи со времени последнего запуска
get_logs() {
  timestamp_old=$(date -d "$OLD_DATE" +%s)
  while IFS= read -r line
  do
    new_date=$(echo "$line" | grep -E -o "\[.*\+0300]" | tr -d " []/" | sed 's/:/ /')
    timestamp_new=$(date -d "$new_date" +%s)
    if [[ timestamp_old -lt timestamp_new ]]; then
      echo "$line"
      # Записываем за какое время был обработан лог
      echo "$timestamp_new" > "$LAST_LOG_DATE"
    fi
  done < "$LOGS_FILES"
}

# Список IP адресов (с наибольшим кол-вом запросов) с указанием кол-ва запросов c момента последнего запуска скрипта
get_often_ip() {
  echo "$LOGS" | awk '{print $1}' | sort | uniq -c | sort -nr | head
}

# Список запрашиваемых URL (с наибольшим кол-вом запросов) с указанием кол-ва запросов c момента последнего запуска скрипта;
get_often_url() {
  echo "$LOGS" | grep -E -o "GET.*HTTP" | sed 's/^GET \(.*\) HTTP$/\1/' | sort | uniq -c | sort -nr | head
}

# Ошибки веб-сервера/приложения c момента последнего запуска скрипта
get_error_list() {
  echo "$LOGS" | grep -E "\s[4-5][0-9]{1,2}\s"
}

# Список всех кодов HTTP ответа с указанием их кол-ва с момента последнего запуска скрипта.
get_status_codes() {
  echo "$LOGS" | grep -E -o "\s[1-5][0-9]{1,2}\s" | sort | uniq -c
}

# Функция для отправки письма
send_mail() {
    if mail -s "The service status" "$EMAIL" < $MESSAGE; then
        echo "Письмо успешно отправлено"
    else
        echo "Ошибка при отправке письма" >&2
        exit 1
    fi
}

# Получаем логи
LOGS=$(get_logs)

# Получаем временной диапазон обработки логов
TIME_RANGE="$OLD_DATE - $(date --date="@$(cat $LAST_LOG_DATE)")"

# Формируем сообщение для отправки
{
  echo "Обрабатываем логи за период:"
  echo "$TIME_RANGE"
  echo "Список IP адресов:"
  get_often_ip
  echo "Список запрашиваемых URL:"
  get_often_url
  echo "Ошибки веб-сервера:"
  get_error_list
  echo "Список всех кодов HTTP ответов:"
  get_status_codes
} >> $MESSAGE

# Запускаем функцию отправки письма
send_mail

# Очистка временных файлов будет выполнена автоматически через trap

#vagrant
### Для Windows

1. Скачать и установить VirtualBox
2. Скачать и установить Vagrant
3. Добавить путь до исполняемого файла Vagrant в PATH
```shell
set PATH=%PATH%;C:\Program Files\Vagrant\bin
```
4.  Проверить, что мы можем использовать Vagrant из консоли
```shell
vagrant -v
```
`>> Vagrant 2.4.1`

5. Вы великолепны!

> [!NOTE]  
> Что дальше?
> [[Получение VagrantBox]], [[Создание виртуальных машин]]
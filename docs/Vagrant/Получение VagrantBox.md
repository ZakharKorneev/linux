#vagrant
1. Перейти на https://app.vagrantup.com/
2. Выбрать нужный образ ОС
3. На локальной машине, в месте, где мы хотим сохранить файл конфигурации выполнить `vagrant init $vagrant_box`, напр.:
```shell
vagrant init ubuntu/jammy64
```
4. Убедиться, что в целевой папке был создан файл `Vagrantfile`
5. Вы великолепны!

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile for creating two Ubuntu machines with specific configurations

Vagrant.configure("2") do |config|
  # Define the first machine named "nginx"
  config.vm.define "nginx" do |node|
    node.vm.box = "ubuntu/focal64"
    node.vm.hostname = "nginx"
    node.vm.network :private_network, ip: "192.168.56.10"
    node.vm.provider :virtualbox do |vb|
      vb.cpus = 2
      vb.memory = 1024
    end
  end

  # Define the second machine named "ansible"
  config.vm.define "ansible" do |node|
    node.vm.box = "ubuntu/focal64"
    node.vm.hostname = "ansible"
    node.vm.network :private_network, ip: "192.168.56.11"
    node.vm.provider :virtualbox do |vb|
      vb.cpus = 2
      vb.memory = 1024
    end
    # Set up a shared folder on the host machine to /vagrant in machine
    config.vm.synced_folder ".", "/vagrant", disabled: false

    # Provision Ansible using shell inline
    node.vm.provision :shell, inline: <<-SHELL
      # Update package list and install Ansible
      sudo apt-get update -y
      sudo apt-get install -y ansible
      cd /vagrant
      # Run the playbook to configure nginx
      ansible-playbook playbook.yml
    SHELL
  end
end

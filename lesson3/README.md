# Первые шаги с Ansible

## Цель домашнего задания

Написать первые шаги с Ansible.

## Описание домашнего задания

Подготовить стенд на Vagrant как минимум с одним сервером. На этом сервере, используя Ansible необходимо развернуть nginx со следующими условиями:
- необходимо использовать модуль yum/apt
- конфигурационный файлы должны быть взяты из шаблона jinja2 с переменными
- после установки nginx должен быть в режиме enabled в systemd
- должен быть использован notify для старта nginx после установки
- сайт должен слушать на нестандартном порту - 8080, для этого использовать переменные в Ansible

*Сделать все это с использованием Ansible роли


## Key Concepts

* **Vagrant:** We'll utilize Vagrant to create and manage virtual machines for development and testing.
* **Ansible:** We'll use Ansible, a powerful configuration management tool, to automate the deployment of Nginx.
* **Roles:** Ansible roles help organize tasks and configurations. The `deploy_nginx` role manages the installation and configuration of Nginx.
* **Playbooks:** Playbooks are YAML files that define the Ansible tasks to be executed.

**How to Proceed:**

1.  **Set Up:**
    *   Ensure Vagrant and Ansible are installed on your system.
    *   Clone this repository: `git clone <repository URL>`
    *   Navigate to the `lesson3` folder: `cd lesson3`

2.  **Start the VMs:**
    *   Launch the virtual machines using `vagrant up`. 

3.  **Verify Nginx:**
    *   Access the Nginx web server by navigating to `http://192.168.56.10:8080` in your browser. 

**Important Files:**

*   `Vagrantfile`: Defines the virtual machine configurations.
*   `playbook.yml`: Executes Ansible tasks to deploy Nginx.
*   `inventory.ini`: Specifies the Ansible hosts.
*   `roles/deploy_nginx/tasks/main.yml`: Ansible tasks for deploying and configuring Nginx.
*   `roles/deploy_nginx/templates/nginx.conf.j2`:  The Nginx configuration template.



**Further Exploration:**

*   Explore the Ansible documentation: [https://docs.ansible.com/](https://docs.ansible.com/)
*   Learn more about Vagrant: [https://www.vagrantup.com/](https://www.vagrantup.com/)



Let me know if you'd like more details on any specific aspect!

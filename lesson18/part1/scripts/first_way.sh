#!/bin/bash
echo -e "\n\n\n"
echo "==================>>The first way<<=================="

echo "Разрешим в SELinux работу nginx на порту TCP 4881 c помощью переключателей setsebool"

echo "Пробуем запустить nginx на нестандартном порту"
systemctl start nginx || true

echo "Проверяем, что nginx не запустился"
if systemctl status nginx | grep "Failed to start The nginx" &> /dev/null; then
  echo "The nginx is not running"
else
  echo "Fail. The nginx state is not expected"
  systemctl status nginx
  exit 1
fi

# Получаем время лога с ошибкой запуска nginx
LOG_TIME=$(grep "/usr/sbin/nginx" /var/log/audit/audit.log | awk '{print $2}' | grep -E -o "[0-9]*\.[0-9]{3}:[0-9]{3}")

echo "Расшифровываем ошибку запуска nginx с помощью утилиты audit2why"
grep "$LOG_TIME" /var/log/audit/audit.log | audit2why # Исходя из вывода утилиты, мы видим, что нам нужно поменять параметр nis_enabled.

echo "Включаем параметр nis_enabled."
setsebool -P nis_enabled on && echo "Ok"

echo "Проверяем статус параметра"
getsebool -a | grep nis_enabled

echo "Перезапускаем nginx."
systemctl restart nginx

echo "Проверяем, что nginx запущен"
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: active (running)" &> /dev/null; then
  echo "The nginx is running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "Проверим, что при переходе по ожидаемому адресу (http://127.0.0.1:4881) получаем 200 КО:"
if curl -I 'http://127.0.0.1:4881' 2> /dev/null | grep -o "HTTP/1.1 200 OK"; then
  echo ""
else
  echo "URL http://127.0.0.1:4881 is not available"
  exit 1
fi

echo "Останавливаем nginx"
systemctl stop nginx && echo "Ok"

echo "Проверяем, что nginx остановлен"
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: inactive (dead)" &> /dev/null; then
  echo "The nginx is not running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "Возвращаем запрет работы nginx на порту 4881."
setsebool -P nis_enabled off && echo "Ok"

echo "Проверяем статус параметра"
getsebool -a | grep nis_enabled

echo "==================>>The first way is done<<=================="
echo -e "\n\n\n"

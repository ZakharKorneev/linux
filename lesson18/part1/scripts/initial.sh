#!/bin/bash
echo -e "\n\n\n"
echo "==================>>Start initial script<<=================="
echo "Set repositories"
cd /etc/yum.repos.d/ || exit 1
sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
cd || exit 1

#install epel-release
yum install -y epel-release
#install nginx
yum install -y nginx
#change nginx port
sed -ie 's/:80/:4881/g' /etc/nginx/nginx.conf
sed -i 's/listen       80;/listen       4881;/' /etc/nginx/nginx.conf
#install SE Tools
yum install -y т setools-console  policycoreutils-python policycoreutils-newrole selinux-policy-mls
echo "==================>>Initial done!<<=================="
echo -e "\n\n\n"

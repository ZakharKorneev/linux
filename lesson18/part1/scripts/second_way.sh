#!/bin/bash
echo -e "\n\n\n"
echo "==================>>The second way<<=================="

echo "Разрешим в SELinux работу nginx на порту TCP 4881 c помощью добавления нестандартного порта в имеющийся тип"

echo "Пробуем запустить nginx на нестандартном порту"
systemctl start nginx || true

echo "Проверяем, что nginx не запустился"
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: failed (Result: exit-code)" &> /dev/null; then
  echo "The nginx is not running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "Получаем имеющиеся типы, для http трафика"
semanage port -l | grep http

echo "Добавим порт в тип http_port_t"
semanage port -a -t http_port_t -p tcp 4881

echo "Проверяем, что порт был успешно добавлен"
HTTP_PORT_T=$(semanage port -l | grep  http_port_t)
if echo "$HTTP_PORT_T" | grep "4881" &> /dev/null; then
  echo "Port is present"
  echo "$HTTP_PORT_T"
else
  echo "Port is not present"
  echo "$HTTP_PORT_T"
  exit 1
fi

echo "Перезапускаем nginx."
systemctl restart nginx

echo "Проверяем, что nginx запущен"
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: active (running)" &> /dev/null; then
  echo "The nginx is running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "Проверим, что при переходе по ожидаемому адресу (http://127.0.0.1:4881) получаем 200 КО:"
if curl -I 'http://127.0.0.1:4881' 2> /dev/null | grep -o "HTTP/1.1 200 OK"; then
  echo ""
else
  echo "URL http://127.0.0.1:4881 is not available"
  exit 1
fi

echo "Останавливаем nginx"
systemctl stop nginx && echo "Ok"

echo "Удаляем нестандартный порт из имеющегося типа"
semanage port -d -t http_port_t -p tcp 4881
semanage port -l | grep http_port_t

echo "Пытаемся повторно запустить nginx"
systemctl restart nginx
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: failed (Result: exit-code)" &> /dev/null; then
  echo "The nginx is not running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "==================>>The second way is done<<=================="
echo -e "\n\n\n"
#!/bin/bash
echo -e "\n\n\n"
echo "==================>>The third way<<=================="

echo "Разрешим в SELinux работу nginx на порту TCP 4881 c помощью формирования и установки модуля SELinux"

echo "Пробуем запустить nginx на нестандартном порту"
systemctl start nginx || true

echo "Проверяем, что nginx не запустился"
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: failed (Result: exit-code)" &> /dev/null; then
  echo "The nginx is not running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "С помощью утилиты audit2allow создаём модуль на основе логов SELinux разрешающий работу nginx на нестандартном порту"
grep nginx /var/log/audit/audit.log | audit2allow -M nginx

echo "Применяем сформированный модуль"
semodule -i nginx.pp

echo "Проверяем, что модуль находится в списке установленных"
semodule -l | grep nginx

echo "Запускаем nginx"
systemctl start nginx

echo "Проверяем, что nginx запущен"
NGINX_STATUS=$(systemctl status nginx)
if echo "$NGINX_STATUS" | grep "Active: active (running)" &> /dev/null; then
  echo "The nginx is running"
  echo "$NGINX_STATUS"
else
  echo "Fail. The nginx state is not expected"
  echo "$NGINX_STATUS"
  exit 1
fi

echo "Проверим, что при переходе по ожидаемому адресу (http://127.0.0.1:4881) получаем 200 КО:"
if curl -I 'http://127.0.0.1:4881' 2> /dev/null | grep -o "HTTP/1.1 200 OK"; then
  echo ""
else
  echo "URL http://127.0.0.1:4881 is not available"
  exit 1
fi

echo "Удаляем модуль"
semodule -r nginx
echo "==================>>The third way is done<<=================="

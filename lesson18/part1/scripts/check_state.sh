#!/bin/bash
echo -e "\n\n\n"
echo "==================>>Checking VM state<<=================="

if  systemctl status firewalld | grep "inactive" > /dev/null; then
  echo "Ok. firewalld is disabled"
else
  echo "Fail. firewalld is running, but expected state is inactive"
  systemctl status firewalld
  exit 1
fi

if nginx -t 2>&1 | grep "successful" > /dev/null; then
  echo "Ok. The test nginx config is successful"
else
  echo "Fail. The nginx config has errors. Check /etc/nginx/nginx.conf"
  exit 1
fi

if getenforce | grep "Enforcing" > /dev/null; then
  echo "Ok. SELinux state is Enforcing"
else
  echo "Fail. Expected SELinux state is Enforcing, but the state is:"
  getenforce
  exit 1
fi

echo "==================>>Checks are passed<<=================="
echo -e "\n\n\n"

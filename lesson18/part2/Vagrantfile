# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # Define the first machine named "ns01"
  config.vm.define "ns01" do |ns01|
    ns01.vm.box = "centos/7"
    ns01.vm.network "private_network", ip: "192.168.50.10", virtualbox__intnet: "dns"
    ns01.vm.hostname = "ns01"
    ns01.vm.provider :virtualbox do |vb|
      vb.cpus = 1
      vb.memory = 512
    end
    # Set up a shared folder on the host machine to /vagrant in machine
    config.vm.synced_folder ".", "/vagrant", disabled: true
    # Generate logs
    ns01.vm.provision :shell, inline: <<-SHELL
        cd /etc/yum.repos.d/
        sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
        sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
#         semanage fcontext --add --type named_zone_t '/etc/named(/.*)?'
#         restorecon -v -R /etc/named
    SHELL
  end

  # Define the second machine named "client"
  config.vm.define "client" do |client|
    client.vm.box = "centos/7"
    client.vm.network "private_network", ip: "192.168.50.15", virtualbox__intnet: "dns"
    client.vm.hostname = "ns01"
    client.vm.provider :virtualbox do |vb|
      vb.cpus = 1
      vb.memory = 512
    end
    # Set up a shared folder on the host machine to /vagrant in machine
    config.vm.synced_folder ".", "/vagrant", disabled: true
    # Generate logs
    client.vm.provision :shell, inline: <<-SHELL
        cd /etc/yum.repos.d/
        sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
        sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
    SHELL
  end

  # Define the third machine named "ansible"
  config.vm.define "ansible" do |ansible|
    ansible.vm.box = "ubuntu/focal64"
    ansible.vm.hostname = "ansible"
    ansible.vm.network "private_network", ip: "192.168.50.20", virtualbox__intnet: "dns"
    ansible.vm.provider :virtualbox do |vb|
      vb.cpus = 1
      vb.memory = 512
    end
    # Set up a shared folder on the host machine to /vagrant in machine
    config.vm.synced_folder ".", "/vagrant", disabled: false

    # Provision Ansible using shell inline
    ansible.vm.provision :shell, inline: <<-SHELL
      # Update package list and install Ansible
      sudo apt-get update
      sudo apt-get install -y ansible
      # Set files path for run ansible
      cp -r -v /vagrant/.vagrant/machines/ /home/vagrant
      sudo chown -R vagrant:vagrant /home/vagrant/machines
      sudo chmod 600 /home/vagrant/machines/ns01/virtualbox/private_key
      sudo chmod 600 /home/vagrant/machines/client/virtualbox/private_key
      mkdir /home/vagrant/ansible
      cp -v -r /vagrant/provisioning/* /home/vagrant/ansible
      sudo chown -R vagrant:vagrant ansible/
      # Run ansible
      cd ansible/
      ansible-galaxy collection install community.general
      ansible-playbook playbook.yml
    SHELL
  end
end

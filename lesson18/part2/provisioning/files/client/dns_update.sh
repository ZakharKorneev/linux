#!/bin/bash
set -e

# Команды для nsupdate
nsupdate -k /etc/named.zonetransfer.key <<EOF
  server 192.168.50.10
  zone ddns.lab
  update add www.ddns.lab. 60 A 192.168.50.15
  send
  quit
EOF
echo "DNS record was update"
dig www.ddns.lab

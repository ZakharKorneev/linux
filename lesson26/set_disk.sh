#!/bin/bash

if pvs | grep "/dev/sdc"; then
  echo "Физический том существует."
else
  pvcreate /dev/sdc
fi

if vgs | grep "backup"; then
  echo "Группа томов 'backup' уже существует."
else
  vgcreate backup /dev/sdc
fi

if lvs | grep borgbackup; then
  echo "Логический том 'borgbackup' существует."
else
  lvcreate -l+100%FREE -n borgbackup backup
  mkfs.ext4 /dev/backup/borgbackup
  mkdir /var/backup
fi

if mount | grep /var/backup; then
  echo "Том смонтирован."
else
  mount /dev/backup/borgbackup /var/backup
  echo "$(blkid | grep borgbackup: | awk '{print $2}') /var/backup ext4 defaults 0 0" >> /etc/fstab
fi

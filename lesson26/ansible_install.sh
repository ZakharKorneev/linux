#!/bin/bash
# Ansible install if it not present
if ! which ansible > /dev/null 2>&1; then
  sudo apt-get update
  sudo apt-get install -y ansible
fi

# Set files path for run ansible
cp -r -v /vagrant/.vagrant/machines/ /home/vagrant
sudo chown -R vagrant:vagrant /home/vagrant/machines
sudo chmod 600 /home/vagrant/machines/client/virtualbox/private_key
sudo chmod 600 /home/vagrant/machines/backup/virtualbox/private_key

if [ ! -d "/home/vagrant/ansible" ]; then
  mkdir /home/vagrant/ansible
fi

cp -v -r /vagrant/ansible/* /home/vagrant/ansible
sudo chown -R vagrant:vagrant ansible/

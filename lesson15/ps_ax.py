import glob
import os
import time
from typing import Union


def get_tty(tty_nr: int, pid: Union[str, int]) -> str:
    """Возвращает строку TTY для данного номера TTY.
    tty_nr: Значение tty_nr из /proc/[PID]/stat
    """
    # Разбиваем на major и minor числа
    major = (tty_nr >> 8) & 0xFF
    minor = tty_nr & 0xFF

    # Ищем устройство с этим major/minor среди "псевдотерминалов"
    device_name = None
    for device in glob.glob('/dev/pts/*'):
        try:
            stat = os.stat(device)
            if os.major(stat.st_rdev) == major and os.minor(stat.st_rdev) == minor:
                device_name = device.lstrip("/dev/")
                break
        except FileNotFoundError:
            continue
    # Если не смогли определить "псевдотерминал", то проверяем поток ввода
    if device_name is None:
        fd_path = f'/proc/{pid}/fd/0'
        try:
            device_name = os.readlink(fd_path).lstrip("/dev/")  # Получаем ссылку на файл дескриптора
            if 'tty' not in device_name:
                return '?'
        except OSError:
            return '?'

    return device_name


def get_proc_stat(pid: Union[str, int], stat_data: list[str]) -> str:
    """ Получаем статус процесса на основе информации из /proc/[PID]/stat
    pid: PID процесса для которого получаем состояние
    stat_data: Список состояний на основе /proc/[PID]/stat
    """
    state: str = stat_data[2]  # состояние процесса (S, R, D, T, Z, X)
    session_id = stat_data[5]  # идентификатор сессии
    terminal = int(stat_data[6])  # терминал процесса (tty_nr)
    nice_value = int(stat_data[18])  # nice значение процесса
    num_threads = int(stat_data[19])  # количество потоков

    # Если процесс имеет приоритет меньше нуля (высокий приоритет)
    if nice_value < 0:
        state += '<'

    # Если процесс является лидером сессии (проверяем session_id == pid)
    if int(session_id) == int(pid):
        state += 's'

    # Если процесс является многопоточным (multi-threaded)
    if num_threads > 1:
        state += 'l'

    # Если процесс на переднем плане (foreground process group)
    if terminal > 0:
        state += '+'

    return state


def get_time(utime: int, stime: int):
    """Возвращает строку времени в формате MM:SS, используя utime и stime."""
    total_time = (utime + stime) / os.sysconf(os.sysconf_names['SC_CLK_TCK'])
    return time.strftime('%M:%S', time.gmtime(total_time))


def get_process_info():
    proc_list = []

    # Чтение информации о процессах из /proc
    for pid in os.listdir('/proc'):
        if pid.isdigit():
            try:
                with open(f'/proc/{pid}/stat', 'r') as f_stat:
                    stat_info: list[str] = f_stat.read().split()

                with open(f'/proc/{pid}/cmdline', 'r') as f_cmdline:
                    cmdline = f_cmdline.read().replace('\0', ' ').strip()

                tty_nr = int(stat_info[6])
                tty = get_tty(tty_nr, pid)
                state = get_proc_stat(pid=pid, stat_data=stat_info)
                time_spent = get_time(int(stat_info[13]), int(stat_info[14]))
                command = cmdline if cmdline else f'[{stat_info[1][1:-1]}]'

                proc_list.append((int(pid), tty, state, time_spent, command))

            except IOError:  # Появление процесса во время обработки
                continue

    return proc_list


def print_processes(proc_list):
    # Заголовок таблицы
    print(f"{'PID':>7} {'TTY':<7} {'STAT':<5} {'TIME':>8} COMMAND")

    # Печать информации о каждом процессе
    for proc in proc_list:
        pid, tty, state, time_spent, command = proc
        print(f"{pid:>7} {tty:<7} {state:<5} {time_spent:>8} {command}")


if __name__ == '__main__':
    processes = get_process_info()
    print_processes(processes)

## Цель домашнего задания
Научиться самостоятельно разворачивать сервис NFS и подключать к нему клиентов.

## Описание домашнего задания 
### Основная часть: 

- [x] vagrant up должен поднимать 2 настроенных виртуальных машины (сервер NFS и клиента) без дополнительных ручных действий;
- [x] на сервере NFS должна быть подготовлена и экспортирована директория; 
- [x] в экспортированной директории должна быть поддиректория с именем upload с правами на запись в неё; 
- [x] экспортированная директория должна автоматически монтироваться на клиенте при старте виртуальной машины (systemd, autofs или fstab — любым способом);
- [x] монтирование и работа NFS на клиенте должна быть организована с использованием NFSv3.

### Для самостоятельной реализации:
~~настроить аутентификацию через KERBEROS с использованием NFSv4.~~


## Key Concepts

* **Vagrant:** We'll utilize Vagrant to create and manage virtual machines for development and testing.
* **Ansible:** We'll use Ansible, a powerful configuration management tool, to automate the deployment of NFS.
* **Roles:** Ansible roles help organize tasks and configurations. The `nfs-server` and `nfs-client` roles manage the installation and configuration of NFS.
* **Playbooks:** Playbooks are YAML files that define the Ansible tasks to be executed.

**How to Proceed:**

1.  **Set Up:**
    *   Ensure Vagrant is installed on your system.
    *   Clone this repository: `git clone <repository URL>`
    *   Navigate to the `lesson9` folder: `cd lesson9`

2.  **Start the VMs:**
    *   Launch the virtual machines using `vagrant up`. 

**Important Files:**

*   `Vagrantfile`: Defines the virtual machine configurations.
*   `playbook.yml`: Executes Ansible tasks to deploy NFS server and client.
*   `inventory.ini`: Specifies the Ansible hosts.
*   `roles/nfs-server/tasks/main.yml`: Ansible tasks for deploying and configuring NFS server.
*   `roles/nfs-client/tasks/main.yml`: Ansible tasks for deploying and configuring NFS client.
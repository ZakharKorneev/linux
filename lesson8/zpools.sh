#!/bin/bash

echo 'Get device list:'
mapfile -t DEVICE_LIST < <(lshw -short | grep '536MB' | grep -E -o '/dev/sd.')
echo "${DEVICE_LIST[*]}"

echo 'Check if there are eight disks'
disk_count=${#DEVICE_LIST[@]}
if [ "$disk_count" == 8 ]
then
  echo "All good!";
else
  echo "Not all disks are present";
  exit 1
fi

echo 'Creating mirrored zpools'
i=0;
while [ $i -lt ${#DEVICE_LIST[@]} ]; do
  zpool create otus${i} ${DEVICE_LIST[$i]} ${DEVICE_LIST[$((i + 1))]}
  i=$((i + 2));
done

echo 'Check zpool list'
mapfile -t ZPOOL_LIST < <(zpool list | grep -E -o 'otus.')
zpools_count=${#ZPOOL_LIST[@]}
if [ "$zpools_count" == 4 ]
then
  zpool list
  echo "All good!";
else
  echo "Not all zpools are present";
  exit 1
fi

echo 'Add different compression algorithms to each file system'
zfs set compression=lzjb otus0
zfs set compression=gzip-9 otus2
zfs set compression=zle otus4
zfs set compression=lz4 otus6
zfs get all | grep compression

echo 'Add data to zpools'
wget https://gutenberg.org/cache/epub/2600/pg2600.converter.log
for i in $(seq 0 2 6);do
  cp pg2600.converter.log /otus$i
done

echo 'Check data'
if [ "$(ls -l /otus* | grep "pg2600.converter.log" | wc -l)" == 4 ]
then
  ls -l /otus*;
  echo "All good!";
else
  echo "Not all files are present";
  exit 1
fi

echo 'Find the best compression algorithm'
zfs list
echo 'Get minimum size of data'
MIN_SIZE_POOL=$(df -h | tail -n +2 | sort -k 3 | grep otus | head -1 | grep -o -E "^otus.")
echo "Minimum size has been found in: $MIN_SIZE_POOL";
echo 'The best compression algorithm is:'
zfs get all | grep compression | grep "$MIN_SIZE_POOL"


echo 'Download archive'
wget -O archive.tar.gz --no-check-certificate 'https://drive.usercontent.google.com/download?id=1MvrcEp-WgAQe57aDEzxSRalPAwbNN1Bb&export=download'
if ls | grep -q archive.tar.gz; then
  echo "File has been downloaded";
else
  echo "File hasn't been downloaded";
  exit 1
fi
echo 'Unzip the archive'
tar -xzvf archive.tar.gz
echo 'Try to import pool from archive'
if zpool import -d zpoolexport/ | grep -q "state: ONLINE"; then
  zpool import -d zpoolexport/ otus
  echo "Pool has been imported";
  zpool status otus
else
  echo "Pool hasn't been imported";
  exit 1
fi

echo 'Check otus zpool parameters:'
echo 'Check available space:'
zfs get available otus
echo 'Check FS:'
zfs get readonly otus
echo 'Check record size:'
zfs get recordsize otus
echo 'Check compression algorithm:'
zfs get compression otus
echo 'Check checksum algorithm:'
zfs get checksum otus

echo 'Get a snapshot'
wget -O otus_task2.file --no-check-certificate "https://drive.usercontent.google.com/download?id=1wgxjih8YZ-cqLqaZVa0lA3h3Y029c3oI&export=download"
if ls | grep -q otus_task2.file; then
  echo "File has been downloaded";
else
  echo "File hasn't been downloaded";
  exit 1
fi

echo 'Recovery the filesystem from the snapshot'
zfs receive otus/test@today < otus_task2.file

echo 'Look for a secret message'
FIND_PATH=$(find /otus/test -name "secret_message")
if [ -n "$FIND_PATH" ]; then
  echo "File has been recovered";
else
  echo "File hasn't been recovered";
  exit 1
fi

echo 'Check the file'
cat $FIND_PATH

### Стенд с Vagrant c ZFS

### 1. Определить алгоритм с наилучшим сжатием:
Определить какие алгоритмы сжатия поддерживает zfs (gzip, zle, lzjb, lz4);
создать 4 файловых системы на каждой применить свой алгоритм сжатия;
для сжатия использовать либо текстовый файл, либо группу файлов.

### 2. Определить настройки пула.
С помощью команды zfs import собрать pool ZFS.

Командами zfs определить настройки:
- размер хранилища;
- тип pool;
- значение recordsize;
- какое сжатие используется;
- какая контрольная сумма используется.

### 3. Работа со снапшотами:
- скопировать файл из удаленной директории;
- восстановить файл локально. zfs receive;
- найти зашифрованное сообщение в файле secret_message.

### Documentation

This lesson covers the basics of ZFS pools, including creating mirrored pools, adding different compression algorithms, and recovering data from snapshots.

**Files:**

* `lesson8/zpools.sh`: The main script for the lesson. It includes tasks for creating pools, adding data, importing pools from an archive, and recovering data from snapshots.
* `lesson8/README.md`: This README provides an overview of the lesson and explains how to use the files.
* `lesson8/lesson8.gitlab-ci.yml`: This file configures the CI/CD pipeline for the lesson.

**Instructions:**

1. `vagrant up` into lesson8 folder for run VM

**Additional Resources:**

* **ZFS documentation:** https://docs.oracle.com/cd/E19253-01/820-0836/index.html
* **GitLab CI/CD documentation:** https://docs.gitlab.com/

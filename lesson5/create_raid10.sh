#!/bin/bash

DEVICE_LIST=$(sudo lshw -short | grep disk | grep -E '/d/[0-9]' | grep -E -o '/dev/sd.')

echo 'Check if there are four disks'
if [ "$($DEVICE_LIST | wc -l)" == 4 ]
then
  echo "All good!";
else
  echo "Not all disks are present";
fi

echo 'Wipe the superblocks of all disks'
for i in $DEVICE_LIST; do
    sudo mdadm --zero-superblock --force "$i"
done

echo "Create a RAID 10 array with the four disks: $(echo "$DEVICE_LIST" | tr '\n' ' ')"
if echo "$DEVICE_LIST" | xargs sudo mdadm --create --verbose /dev/md0 -l 10 -n 4
then
    echo "Successfully created RAID 10 array";
else
    echo "Failed to create RAID 10 array";
    exit 1;
fi

echo 'Check if the mdadm is running:'
cat /proc/mdstat
sudo mdadm -D /dev/md0

if grep md0 /proc/mdstat | grep 'active'
then
    echo "RAID array is active";
else 
    echo "RAID array is not active"; 
    exit 1;
fi

if ! grep 'UUUU' /proc/mdstat
then
    echo "RAID array is brocken";
fi

echo 'Create the configuration file for mdadm'
echo "DEVICE partitions" > /etc/mdadm/mdadm.conf
sudo mdadm --detail --scan --verbose | sudo awk '/ARRAY/ {print}' >> /etc/mdadm/mdadm.conf

echo 'To break the RAID'
BROKEN_DISC=$(echo "$DEVICE_LIST" | shuf -n 1)

echo "To fail one of the disks in the array"
sudo mdadm /dev/md0 --fail "$BROKEN_DISC"
echo "Breaking disk: $BROKEN_DISC"

cat /proc/mdstat
sudo mdadm -D /dev/md0

if sudo mdadm -D /dev/md0 | grep faulty
then
    echo "Disk is faulty";
else
    echo "Disk is not faulty";
    exit 1;
fi

echo "Remove the broken disk from the array"
sudo mdadm /dev/md0 --remove "$BROKEN_DISC"

sudo mdadm -D /dev/md0

sudo mdadm /dev/md0 --add "$BROKEN_DISC"

cat /proc/mdstat
sudo mdadm -D /dev/md0


echo 'Waiting for the array will be healthy'
for i in $(seq 1 12); do
    active_disks=$(sudo mdadm -D /dev/md0 | grep -c 'active')
    if [ "$active_disks" == 4 ]
        then
            echo "RAID array is healthy";
            break
        else
            echo "RAID array is not healthy";
            echo "An attempt to verify $i from 12"
            sleep 5;
    fi
done

echo "Create a new gpt record on the RAID array"
sudo parted -s /dev/md0 mklabel gpt

echo "Create a new partitions on the RAID array"
parted /dev/md0 mkpart primary ext4 0% 20%
parted /dev/md0 mkpart primary ext4 20% 40%
parted /dev/md0 mkpart primary ext4 40% 60%
parted /dev/md0 mkpart primary ext4 60% 80%
parted /dev/md0 mkpart primary ext4 80% 100%

echo "Create file systems on the partitions"
for i in $(seq 1 5); do 
    sudo mkfs.ext4 "/dev/md0p$i"
done 

echo "Mount the partitions"
mkdir -p /raid/part{1,2,3,4,5}
for i in $(seq 1 5); do 
    sudo mount "/dev/md0p$i" "/raid/part$i"
done

echo "All partitions are mounted"
echo "All done!"
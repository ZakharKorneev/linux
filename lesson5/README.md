## Домашнее задание: работа с mdadm

## Задание:

- добавить в Vagrantfile еще дисков

- собрать R0/R5/R10 на выбор

- прописать собранный рейд в конф, чтобы рейд собирался при загрузке

- сломать/починить raid 

- создать GPT раздел и 5 партиций и смонтировать их на диск.

В качестве проверки принимается - измененный Vagrantfile, скрипт для создания рейда, конф для автосборки рейда при загрузке.

* Доп. задание - Vagrantfile, который сразу собирает систему с подключенным рейдом

Vagrantfile:

The Vagrantfile is responsible for setting up the virtual machine environment. It defines the virtual machine configuration, including the box type, IP address, and disks.

lesson5.gitlab-ci.yml:

The lesson5.gitlab-ci.yml file is a GitLab CI/CD pipeline configuration file. It defines the pipeline stages, including the build and cleanup stages.

create_raid10.sh:

The create_raid10.sh script automates the creation of a RAID 10 array. It includes steps for wiping superblocks, initializing the RAID array, checking for errors, and partitioning the RAID array.
